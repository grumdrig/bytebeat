var NA = 0;
var LTR = 1;
var RTL = 2;

var PREFIX = 1;
var INFIX = 2;
var SUFFIX = 4;
var TERNARY = 8;

var OPS = [
  ['.', 1, LTR, SUFFIX],
  ['[', 1, LTR, SUFFIX, ']'],
  ['(', 2, LTR, SUFFIX, ')'],
  ['++', 3, NA, PREFIX | SUFFIX],
  ['--', 3, NA, PREFIX | SUFFIX],
  ['!', 4, RTL, PREFIX],
  ['~', 4, RTL, PREFIX],
  ['+', 4, RTL, PREFIX],
  ['-', 4, RTL, PREFIX],
  ['*', 5, LTR, INFIX],
  ['/', 5, LTR, INFIX],
  ['%', 5, LTR, INFIX],
  ['+', 6, LTR, INFIX],
  ['-', 6, LTR, INFIX],
  ['<<', 7, LTR, INFIX],
  ['>>', 7, LTR, INFIX],
  ['>>>', 7, LTR, INFIX],
  ['<', 8, LTR, INFIX],
  ['<=', 8, LTR, INFIX],
  ['>', 8, LTR, INFIX],
  ['>=', 8, LTR, INFIX],
  ['==', 9, LTR, INFIX],
  ['!=', 9, LTR, INFIX],
  ['===', 9, LTR, INFIX],
  ['!==', 9, LTR, INFIX],
  ['&', 10, LTR, INFIX],
  ['^', 11, LTR, INFIX],
  ['|', 12, LTR, INFIX],
  ['&&', 13, LTR, INFIX],
  ['||', 14, LTR, INFIX],
  ['?', 15, RTL, TERNARY, ':'],
  ['=', 16, RTL, INFIX],
  ['+=', 16, RTL, INFIX],
  ['-=', 16, RTL, INFIX],
  ['*=', 16, RTL, INFIX],
  ['/=', 16, RTL, INFIX],
  ['%=', 16, RTL, INFIX],
  ['<<=', 16, RTL, INFIX],
  ['>>=', 16, RTL, INFIX],
  ['>>>=', 16, RTL, INFIX],
  ['&=', 16, RTL, INFIX],
  ['^=', 16, RTL, INFIX],
  ['|=', 16, RTL, INFIX],
  [',', 17, LTR, INFIX]];

function parse(expr) {
  console.log('EXPR', expr);
  var tokens = [];
  expr = trim(expr);
  while (expr.length > 0) {
    var bestn = expr.length + 1, bestop = '';
    function maybe(op) {
      var n = expr.indexOf(op);
      if (n >= 0 && (n < bestn || (n === bestn && 
                                   op.length > bestop.length))) {
        bestn = n;
        bestop = op;
      }
    }
    for (var i = 0; i < OPS.length; ++i) {
      maybe(OPS[i][0]);
      if (OPS[i][4]) maybe(OPS[i][4]);
    }
    if (!bestop.length) {
      tokens.push(expr);
      expr = '';
    } else {
      if (bestn > 0)
        tokens.push(trim(expr.substr(0, bestn)));
      tokens.push(bestop);
      expr = trim(expr.substr(bestn + bestop.length));
    }
  }
  console.log('TOK', tokens);

  return parseExpr(tokens, 100);
}


function matchOp(token, precedence, position) {
  for (var i = 0; i < OPS.length; ++i) {
    var op = OPS[i];
    if (op[0] === token && (op[3] & position) && 
        ((op[1] < precedence) || (op[1] === precedence && op[2] === RTL)))
      return op;
  }
}


function parseExpr(tokens, precedence) {
  if (!precedence) precedence = 100;
  
  var op = matchOp(tokens[0], precedence, PREFIX);
  var result;
  if (op) {
    result = { operator: tokens.shift(),
               right: parseExpr(tokens, op[1]) };
  } else {
    result = tokens.shift();
    if (result === '(') {
      result = parseExpr(tokens);
      tokens.shift();  // presumably the close paren
    }
  }

  while (tokens.length) {
    var op = matchOp(tokens[0], precedence, INFIX);
    if (!op) break;
    result = { left: result,
               operator: tokens.shift(),
               right: parseExpr(tokens, op[1]) };
  }

  while (tokens.length) {
    var op = matchOp(tokens[0], precedence, SUFFIX);
    if (!op) break;
    result = { left: result,
               operator: tokens.shift() };
    if (op[4]) tokens.shift(); // presumably the matching symbol
  }

  return result;
}

function trim(s) {
  return (''+s).replace(/^\s+|\s+$/g,"");
}

function show(tree, indent) {
  indent = indent || '';
  if (typeof tree !== 'object') {
    console.log(indent, tree);
    return;
  }
  console.log(indent, tree.operator);
  if (tree.left) show(tree.left, indent + '  ');
  if (tree.right) show(tree.right, indent + '  ');
}

show(parse('5 + 6'));
show(parse('((t<<1)^((t<<1)+(t>>7)&t>>12))|t>>(4-(1^7&(t>>19)))|t>>7'));


